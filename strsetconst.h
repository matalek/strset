/* Moduł strsetconst - interfejs
 * mm347080 & am347171
 */
  
#ifdef __cplusplus
extern "C" {
#endif

#include "strset.h"

/* Stała będąca identyfikatorem zbioru, którego nie można modyfikować
 * i który zawiera jeden element: napis "42". */
extern const unsigned long strset42;

#ifdef __cplusplus
}
#endif
