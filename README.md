## Zadanie 2. ##
### Języki i narzędzia programowania I ###
### Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego ###

Biblioteka standardowa języka C++ udostępnia bardzo przydatne
kontenery (np. map i deque), których nie ma w bibliotece C. 

Często potrzebujemy łączyć kod C++ z kodem w C. Celem tego zadania jest
napisanie w C++ dwóch modułów obsługujących zbiory (std::set) ciągów znaków
tak, aby można ich było używać w C. Każdy moduł składa się z pliku
nagłówkowego (z rozszerzeniem h) i pliku z imlementacją (z rozszerzeniem cc).

Moduł strset (pliki strset.h i strset.cc) powinien udostępniać
następujące funkcje:

unsigned long strset_new();

      Tworzy nowy zbiór i zwraca jego identyfikator.

void strset_delete(unsigned long id);

      Jeżeli istnieje zbiór o identyfikatorze id, usuwa go, a w przeciwnym
      przypadku nic nie robi.

size_t strset_size(unsigned long id);

      Jeżeli istnieje zbiór o identyfikatorze id, zwraca liczbę jego
      elementów, a w przeciwnym przypadku zwraca 0.

void strset_insert(unsigned long id, const char* value);

      Jeżeli istnieje zbiór o identyfikatorze id oraz element value
      jeszcze w nim nie istnieje, to go dodaje. W przeciwnym razie
      funkcja nie robi nic.

void strset_remove(unsigned long id, const char* value);

      Jeżeli istnieje zbiór o identyfikatorze id i element value w nim istnieje,
      to usuwa go, a w przeciwnym przypadku nie robi nic.

int strset_test(unsigned long id, const char* value);

      Jeżeli istnieje zbiór o identyfikatorze id i element value w nim istnieje,
      to funkcja zwraca 1, w przeciwnym przypadku 0.

void strset_clear(unsigned long id);

      Jeżeli istnieje zbiór o identyfikatorze id, usuwa wszystkie jego elementy,
      a w przeciwnym przypadku nic nie robi.
      
int strset_comp(unsigned long id1, unsigned long id2);

      Porównuje zbiory id1 i id2. Niech sorted(id) oznacza posortowany
      leksykograficznie zbiór id. Takie ciągi już porównywnujemy naturalnie:
      pierwsze miejsce, na którym się różnią, decyduje o relacji większości.
      Jeśli jeden ciąg jest prefiksem drugiego, to prefiks jest mniejszy.
      Zatem strset_comp(id1, id2) powinno zwrócić
	    -1, gdy sorted(id1) < sorted(id2),
	    0, gdy sorted(id1) = sorted(id2),
	    1, gdy sorted(id1) > sorted(id2).
      Jeżeli zbiór o którymś z identyfikatorów nie istnieje, to jest traktowany
      jako równy zbiorowi pustemu.

Do implementacji zbiorów należy użyć standardowego typu std::set<std::string>. W szczególności
nie należy przechowywać przekazanych przez użytkownika wskaźników const char* bezpośrednio,
bowiem użytkownik może po wykonaniu operacji modyfikować dane pod uprzednio przekazanym
wskaźnikiem lub zwolnić pamięć. Na przykład poniższy kod nie powinien przerwać się z powodu
niespełnionej asercji:

    unsigned long s;
    char buf[4] = "foo";
    s = strset_new();
    strset_insert(s, buf);
    buf[0] = 'b';
    assert(strset_test(s, "foo"));
    assert(!strset_test(s, "boo"));
  
Należy też ukryć przed światem zewnętrznym wszystkie zmienne globalne
i funkcje pomocnicze nie należące do wyspecyfikowanego interfejsu modułu.

Moduł strsetconst (pliki strsetconst.h i strsetconst.cc) powinien
udostępniać stałą:

const unsigned long strset42;

    Powyższa stała jest identyfikatorem zbioru, którego nie można modyfikować
    i który zawiera jeden element: napis "42".

Należy też ukryć przed światem zewnętrznym wszystkie zmienne globalne
i funkcje pomocnicze nie należące do wyspecyfikowanego interfejsu modułu.

Moduły mają być kompilowane w dwóch wersjach.

* Wersja diagnostyczna (debuglevel=1) powinna:
    ** zawierać informację uruchomieniową pozwalającą otworzyć tekst źródłowy w debugerze,
    ** być kompilowana bez optymalizacji,
    ** możliwie dokładnie sprawdzać przekazane argumenty,
    ** wypisywać informacje uruchomieniowe na standardowy strumień błędów.

* Wersja zoptymalizowana (debuglevel=0) powinna:
    ** być pozbawiona wszelkich informacji uruchomieniowych,
    ** być optymalizowana ze względu na szybkość wykonywania,
    ** zakładać, że funkcje wywoływane są poprawnie.

Rozwiązanie powinno zawierać:

* pliki strset.h, strset.cc, strsetconst.h, strsetconst.cc,
* skrypt makefile umożliwiający skompilowanie modułów, tworzący odpowiednio 
  pliki strset.o i strsetconst.o; uruchomienie 'make' lub 'make debuglevel=0'
  powinno generować wersję zoptymalizowaną, a uruchomienie 'make debuglevel=1'
  powinno generować wersję diagnostyczną.

Przykład użycia (wydruk pliku strset_test1.c):

```
#include <assert.h>
#include <stdio.h>
#include "strset.h"
#include "strsetconst.h"

int main() {
    unsigned long s1, s2, s3;

    s1 = strset_new();
    strset_insert(s1, "foo");
    assert(strset_test(s1, "foo"));
    assert(!strset_test(s1, "bar"));
    strset_insert(s1, "bar");
    assert(strset_test(s1, "bar"));
    assert(strset_size(s1) == 2);
    strset_insert(s1, "bar");
    assert(strset_size(s1) == 2);
    strset_remove(s1, "foo");
    assert(!strset_test(s1, "foo"));
    assert(strset_test(s1, "bar"));
    assert(strset_size(s1) == 1);

    strset_delete(s1);
    strset_insert(s1, "whatever");
    assert(strset_size(s1) == 0);
    assert(!strset_test(s1, "whatever"));

    s2 = strset_new();
    s3 = strset_new();
    strset_insert(s2, "Ania");
    strset_insert(s2, "Alek");
    strset_insert(s2, "Maria");
    strset_insert(s2, "Fiona");
    strset_insert(s3, "Ania");
    strset_insert(s3, "Maria");
    assert(strset_comp(s2, s3) == -1);
    assert(strset_comp(s3, s2) == 1);
    strset_remove(s2, "Alek");
    strset_remove(s2, "Fiona");
    strset_remove(s2, "Olek");
    assert(strset_comp(s3, s2) == 0);
    strset_clear(s3);
    assert(strset_comp(s2, s3) == 1);
    strset_clear(s2);
    assert(strset_comp(s2, s3) == 0);
    strset_delete(s2);
    strset_delete(s3);
    assert(strset_comp(s2, s3) == 0);

    assert(strset_size(strset42) == 1);
    strset_delete(strset42);
    strset_insert(strset42, "66");
    assert(strset_size(strset42) == 1);
    strset_insert(strset42, "24");
    strset_insert(strset42, "42");
    assert(!strset_test(strset42, "24"));
    assert(strset_test(strset42, "42"));
    strset_remove(strset42, "42");
    assert(strset_size(strset42) == 1);
    strset_clear(strset42);
    assert(strset_size(strset42) == 1);

    return 0;
}
```

Plik strset_test1.c można skompilować za pomocą polecenia

gcc -Wall -O2 -c strset_test1.c -o strset_test1.o

i połączyć z pozostałymi modułami za pomocą polecenia

g++ strset_test1.o strsetconst.o strset.o -o strset

Przykład informacji diagnostycznych wypisywanych przez powyższy
przykład użycia:

```
strsetconst init invoked
strset_new()
strset_new: the Set 42 created
strsetconst init finished
strset_new()
strset_new: set 1 created
strset_insert(1, "foo")
strset_insert: set 1, element "foo" inserted
strset_test(1, "foo")
strset_test: set 1 contains the element "foo"
strset_test(1, "bar")
strset_test: set 1 does not contain the element "bar"
strset_insert(1, "bar")
strset_insert: set 1, element "bar" inserted
strset_test(1, "bar")
strset_test: set 1 contains the element "bar"
strset_size(1)
strset_size: set 1 contains 2 element(s)
strset_insert(1, "bar")
strset_insert: set 1, element "bar" was already present
strset_size(1)
strset_size: set 1 contains 2 element(s)
strset_remove(1, "foo")
strset_remove: set 1, element "foo" removed
strset_test(1, "foo")
strset_test: set 1 does not contain the element "foo"
strset_test(1, "bar")
strset_test: set 1 contains the element "bar"
strset_size(1)
strset_size: set 1 contains 1 element(s)
strset_delete(1)
strset_delete: set 1 deleted
strset_insert(1, "whatever")
strset_insert: set 1 does not exist
strset_size(1)
strset_size: set 1 does not exist
strset_test(1, "whatever")
strset_test: set 1 does not exist
strset_new()
strset_new: set 2 created
strset_new()
strset_new: set 3 created
strset_insert(2, "Ania")
strset_insert: set 2, element "Ania" inserted
strset_insert(2, "Alek")
strset_insert: set 2, element "Alek" inserted
strset_insert(2, "Maria")
strset_insert: set 2, element "Maria" inserted
strset_insert(2, "Fiona")
strset_insert: set 2, element "Fiona" inserted
strset_insert(3, "Ania")
strset_insert: set 3, element "Ania" inserted
strset_insert(3, "Maria")
strset_insert: set 3, element "Maria" inserted
strset_comp(2, 3)
strset_comp: result of comparing set 2 to set 3 is -1
strset_comp(3, 2)
strset_comp: result of comparing set 3 to set 2 is 1
strset_remove(2, "Alek")
strset_remove: set 2, element "Alek" removed
strset_remove(2, "Fiona")
strset_remove: set 2, element "Fiona" removed
strset_remove(2, "Olek")
strset_remove: set 2 does not contain the element "Olek"
strset_comp(3, 2)
strset_comp: result of comparing set 3 to set 2 is 0
strset_clear(3)
strset_clear: set 3 cleared
strset_comp(2, 3)
strset_comp: result of comparing set 2 to set 3 is 1
strset_clear(2)
strset_clear: set 2 cleared
strset_comp(2, 3)
strset_comp: result of comparing set 2 to set 3 is 0
strset_delete(2)
strset_delete: set 2 deleted
strset_delete(3)
strset_delete: set 3 deleted
strset_comp(2, 3)
strset_comp: set 2 does not exist
strset_comp: set 3 does not exist
strset_size(the 42 Set)
strset_size: the 42 Set contains 1 element(s)
strset_delete(the 42 Set)
strset_delete: attempt to remove the 42 Set
strset_insert(the 42 Set, "66")
strset_insert: attempt to insert into the 42 Set
strset_size(the 42 Set)
strset_size: the 42 Set contains 1 element(s)
strset_insert(the 42 Set, "24")
strset_insert: attempt to insert into the 42 Set
strset_insert(the 42 Set, "42")
strset_insert: attempt to insert into the 42 Set
strset_test(the 42 Set, "24")
strset_test: the 42 Set does not contain the element "24"
strset_test(the 42 Set, "42")
strset_test: the 42 Set contains the element "42"
strset_remove(the 42 Set, "42")
strset_remove: attempt to remove from the 42 Set
strset_size(the 42 Set)
strset_size: the 42 Set contains 1 element(s)
strset_clear(the 42 Set)
strset_clear: attempt to clear the 42 Set
strset_size(the 42 Set)
strset_size: the 42 Set contains 1 element(s)
```

Oczekiwane rozwiązanie powinno korzystać z kontenerów i metod
udostępnianych przez standardową bibliotekę C++. Nie należy definiować
własnych struktur lub klas. W rozwiązaniu nie należy nadużywać kompilacji
warunkowej. Fragmenty tekstu źródłowego realizujące wyspecyfikowane
operacje na zbiorach nie powinny zależeć od sposobu kompilacji (inaczej
posiadanie wersji diagnostycznej nie miałoby sensu).

Pliki rozwiązania należy umieścić w repozytorium w katalogu

grupaN/zadanie2/ab123456+cd123456

lub

grupaN/zadanie2/ab123456+cd123456+ef123456

gdzie N jest numerem grupy, a ab123456, cd123456, ef123456 są identyfikatorami
członków zespołu umieszczającego to rozwiązanie.
Katalog z rozwiązaniem nie powinien zawierać innych plików, ale może zawierać
podkatalog private, gdzie można umieszczać różne pliki, np. swoje testy. Pliki
umieszczone w tym podkatalogu nie będą oceniane.
