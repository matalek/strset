/* Moduł strset - interfejs
 * mm347080 & am347171
 * 
 * Moduł umożliwia tworzenie do 4 000 000 000 zbiorów i przyznaje
 * zbiorom unikalne identyfikatory.
 */
  
#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

/*  Tworzy nowy zbiór i zwraca jego identyfikator. */
unsigned long strset_new();

/* Jeżeli istnieje zbiór o identyfikatorze id, usuwa go, a w przeciwnym
 * przypadku nic nie robi. */
void strset_delete(unsigned long id);

/* Jeżeli istnieje zbiór o identyfikatorze id, zwraca liczbę jego
 * elementów, a w przeciwnym przypadku zwraca 0. */
size_t strset_size(unsigned long id);

/* Jeżeli istnieje zbiór o identyfikatorze id oraz element value
 * jeszcze w nim nie istnieje, to go dodaje. W przeciwnym razie
 * funkcja nie robi nic. */
void strset_insert(unsigned long id, const char* value);

/* Jeżeli istnieje zbiór o identyfikatorze id i element value w nim 
 * istnieje, to usuwa go, a w przeciwnym przypadku nie robi nic. */
void strset_remove(unsigned long id, const char* value);

/* Jeżeli istnieje zbiór o identyfikatorze id i element value w nim
 * istnieje, to funkcja zwraca 1, w przeciwnym przypadku 0. 
 * Jeżeli value nie reprezentuje poprawnego napisu (jest pustym 
 * wskaźnikiem) i wykonywana jest wersja diagnostyczna, to funkcja 
 * zwraca -1. */
int strset_test(unsigned long id, const char* value);

/* Jeżeli istnieje zbiór o identyfikatorze id, usuwa wszystkie jego 
 * elementy, a w przeciwnym przypadku nic nie robi. */
void strset_clear(unsigned long id);

/*  Porównuje zbiory id1 i id2. Niech sorted(id) oznacza posortowany
 * leksykograficznie zbiór id. Takie ciągi już porównywnujemy naturalnie:
 * pierwsze miejsce, na którym się różnią, decyduje o relacji większości.
 * Jeśli jeden ciąg jest prefiksem drugiego, to prefiks jest mniejszy.
 * Zatem strset_comp(id1, id2) powinno zwrócić
 * 	-1, gdy sorted(id1) < sorted(id2),
 * 	0, gdy sorted(id1) = sorted(id2),
 * 	1, gdy sorted(id1) > sorted(id2).
 * Jeżeli zbiór o którymś z identyfikatorów nie istnieje, to jest traktowany
 * jako równy zbiorowi pustemu. */
int strset_comp(unsigned long id1, unsigned long id2);

#ifdef __cplusplus
}
#endif
