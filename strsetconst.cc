/* Moduł strsetconst - implementacja
 * mm347080 & am347171
 */ 

#include <cstdlib>
#include <iostream>
#include <set>
#include <string>
#include "strset.h"
#include "strsetconst.h"

using std::cerr;

//zmienna wskazująca, czy wykonywana jest wersja diagnostyczna
#ifdef NDEBUG
	const bool debug = false;
#else
	const bool debug = true;
#endif

/* funkcja inicjująca zbiór stały o jedynym elemencie równym "42" i 
 * zwracająca jego identyfikator */
static unsigned long strset42_init() {
	if (debug)
		cerr << "strsetconst init invoked\n";
	unsigned long id = strset_new();
	if (debug)
		cerr << "strsetconst init finished\n";
	return id;
}

const unsigned long strset42 = strset42_init();
