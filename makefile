# mm347080 & am347171
# Ustawienie debuglevel=0 kompiluje wersję zoptymalizowaną, natomiast
# debuglevel=1 kompiluje wersję diagnostyczną. W celu przekompilowania
# modułów z inną wersją należy wpierw wywołać polecenie make clean.
CXX = g++
CXXFLAGS = -std=c++11 -Wall

debuglevel ?= 0
ifeq ($(debuglevel), 1)
    	CPPFLAGS= -UNDEBUG
	CXXFLAGS+= -g	
else
	CPPFLAGS= -DNDEBUG
	CXXFLAGS+= -O2
endif


all: strsetconst.o strset.o
strsetconst.o: strsetconst.cc strsetconst.h strset.cc strset.h
strset.o: strsetconst.cc strsetconst.h strset.cc strset.h
clean:
	rm -rf strset.o strsetconst.o 
