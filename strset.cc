/* Moduł strset - implementacja
 * mm347080 & am347171
 */

#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include "strset.h"
#include "strsetconst.h"

using namespace std;

//zmienna wskazująca, czy wykonywana jest wersja diagnostyczna
#ifdef NDEBUG
	const bool debug = false;
#else
	const bool debug = true;
#endif

//funkcja zwracająca statyczny obiekt reprezentujący mapę setów
static map <unsigned long, set <string> >& get_sets_map() {
	static map <unsigned long, set <string> > sets_map;
	return sets_map;
}

/* zwraca id danego seta lub nazwę "the 42 Set", jeśli jest to
 * set stały */
static inline string get_name(unsigned long id) {
	return (id == strset42) ? "the 42 Set" : to_string(id);
}

/* zwraca napise "set" i id danego seta lub nazwę "the 42 Set", 
 * jeśli jest to set stały */
static inline string get_full_name(unsigned long id) {
	return (id == strset42) ? "the 42 Set" : "set " + to_string(id);
}

unsigned long strset_new() {
	//jakie będzie nowo przyznane id
	static unsigned long new_id = 0;
	if (debug) {
		cerr << "strset_new()\n";
		if (new_id != 0)
			cerr << "strset_new: set " << new_id << " created\n";
		else
			cerr << "strset_new: the Set 42 created\n";
	}
	set <string> s;
	/* jeśli tworzymy strset42 - będzie to pierwszy utworzony zbiór,
	 * to od razu wstawiamy do niego wartość 42 */
	if (new_id == 0)
		s.insert("42");
	get_sets_map().insert(make_pair(new_id, s));
	return new_id++;
}

void strset_delete(unsigned long id) {
	if (debug)
		cerr << "strset_delete(" << get_name(id) << ")\n";
	if (id == strset42) {
		if (debug)
			cerr << "strset_delete: attempt to remove the 42 Set\n";
		return;
	}
	
	auto it = get_sets_map().find(id);
	if (it != get_sets_map().end()) {
		get_sets_map().erase(it);
		if (debug)
			cerr << "strset_delete: set " << id << " deleted\n";
	} else if (debug)
		cerr << "strset_delete: set" << id << " does not exist\n";
}

size_t strset_size(unsigned long id) {
	if (debug)
		cerr << "strset_size(" << get_name(id) << ")\n";
		
	auto it = get_sets_map().find(id);
	if (it != get_sets_map().end()) {
		if (debug)
			cerr << "strset_size: " << get_full_name(id) << " contains " 
				<< it->second.size() << " element(s)\n";
		return it->second.size();
	} else { 
		if (debug)
			cerr << "strset_size: set " << id << " does not exist\n";
		return 0;
	}	
}

void strset_insert(unsigned long id, const char* value) {
	if (debug) {
		if (value == nullptr) {
			cerr << "strset_insert: value is null\n";
			return;
		}
		cerr << "strset_insert(" << get_name(id) << ", \"" << 
			value << "\")\n";		
	}	
	if (id == strset42) {
		 if (debug)
			 cerr << "strset_insert: attempt to insert into the 42 Set\n";
		 return;
	}
	
	string text(value);	
	auto it = get_sets_map().find(id);
	if (it != get_sets_map().end()) {
		//sprawdzamy, czy tego elementu nie było wcześniej w secie
		if (it->second.find(text) == it->second.end()) {
			it->second.insert(text);
			if (debug)
				cerr << "strset_insert: set " << id << ", element \"" 
					<< text << "\" inserted\n";
		} else
			if (debug)
				cerr << "strset_insert: set " << id << ", element \"" 
					<< text << "\" was already present\n";
	} else if (debug) {
		cerr << "strset_insert: set " << id << " does not exist\n";
	}	
}

void strset_remove(unsigned long id, const char* value) {
	if (debug) {
		if (value == nullptr) {
			cerr << "strset_remove: value is null\n";
			return;
		}
		cerr << "strset_remove(" << get_name(id) << ", \"" << 
			value << "\")\n";
	}
	if (id == strset42) {
		if (debug)
			cerr << "strset_remove: attempt to remove from the 42 Set\n";	
		return;
	}
		
	string text(value);
	auto it = get_sets_map().find(id);
	if (it != get_sets_map().end()) {
		//sprawdzamy, czy ten element był wcześniej w secie
		if (it->second.find(text) != it->second.end()) {
			it->second.erase(text);
			if (debug)
				cerr << "strset_remove: set " << id << ", element \"" 
					<< text << "\" removed\n"; 
		} else if (debug)
			cerr << "strset_remove: set " << id << 
				" does not contain the element \"" << text << "\"\n"; 
	} else if (debug)
		cerr << "strset_remove: set " << id << " does not exist/n";
}

int strset_test(unsigned long id, const char* value) {
	if (debug) {
		if (value == nullptr) {
			cerr << "strset_test: value is null\n";
			return -1;
		}
		cerr << "strset_test(" << get_name(id) << ", \"" << value << "\")\n";
	}
	
	string text(value);
	auto it = get_sets_map().find(id);
	if (it != get_sets_map().end()) {
		if (it->second.find(text) != it->second.end()) {
			if (debug)
				cerr << "strset_test: " << get_full_name(id) << 
					" contains the element \"" << text << "\"\n";
			return 1;			
		} else if (debug)
			cerr << "strset_test: " << get_full_name(id) 
				<< " does not contain the element \"" 
				<< text << "\"\n";
	} else if (debug) 
		cerr << "strset_test: set " << id << " does not exist\n";
	return 0;
}

void strset_clear(unsigned long id) {
	if (debug)
		cerr << "strset_clear(" << get_name(id) << ")\n";
	if (id == strset42) {
		if (debug)
			cerr << "strset_clear: attempt to clear the 42 Set\n";
		return;
	}
		 
	auto it = get_sets_map().find(id);
	if (it != get_sets_map().end()) {
		it->second.clear();
		if (debug)
			cerr << "strset_clear: set " << id << " cleared\n";
	} else if (debug)
		cerr << "strset_clear: set " << id << " does not exist\n";		
}

int strset_comp(unsigned long id1, unsigned long id2) {
	if (debug)
		cerr << "strset_comp(" << get_name(id1) << ", " << 
			get_name(id2) << ")\n";
	
	auto it1 = get_sets_map().find(id1), it2 = get_sets_map().find(id2);
	//przypadki, gdy jeden ze zbiorów nie istnieje 
	if (it1 == get_sets_map().end() && 
		it2 == get_sets_map().end()) { //jeśli oba sety nie istnieją 
		if (debug)
			cerr << "strset_comp: set " << id1 << " does not exist\n" 
			<< "strset_comp: set " << id2 << " does not exist\n" ;
		return 0;
	}
	if (it1 == get_sets_map().end()) { //jeśli pierwszy set nie istnieje 
		if (debug)
			cerr << "strset_comp: set " << id1 << " does not exist\n";
		//uwzględniamy przypadek, gdy drugi zbiór jest pusty 
		return (it2->second.empty()) ? 0 : -1;
	}
	if (it2 == get_sets_map().end()) { //jeśli drugi set nie istnieje 
		if (debug)
			cerr << "strset_comp: set " << id2 << " does not exist\n";
		//uwzględniamy przypadek, gdy pierwszy zbiór jest pusty 
		return (it1->second.empty()) ? 0 : 1;
	}
		
	auto it_in1 = it1->second.begin(), it_in2 = it2->second.begin(),
		it_end1 = it1->second.end(), it_end2 = it2->second.end();
	//porównywanie kolejnych elementów
	while (it_in1 != it_end1 && it_in2 != it_end2 && 
		*it_in1 == *it_in2) {
		it_in1++;
		it_in2++;
	}
	
	int res; //trzyma wynik, żeby go potem ewentualnie wypisać na stderr
	//jeśli jeden był prefiksem drugiego albo były równe
	if (it_in1 == it_end1 && it_in2 == it_end2)
		res = 0;
	else if (it_in1 == it_end1)
		res = -1;
	else if (it_in2 == it_end2)
		res = 1;
	//jeśli nie były swoimi prefiksami
	else
		res = (*it_in1 < *it_in2) ? -1 : 1;	
	
	if (debug)
		cerr << "strset_comp: result of comparing " << 
			get_full_name (id1) << " to " << get_full_name(id2) 
			<< " is " << res << "\n";
	return res;
}
